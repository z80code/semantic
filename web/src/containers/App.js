import React, {Component} from 'react'

import {Route} from 'react-router-dom';

import NavBar from "../pages/component/NavBar";
import Home from "../pages/Home";
import List from "../pages/List";
import Resource from "../pages/Resource";

export default class App extends Component {
   constructor(props) {
      super(props);
   }

   render() {
      return (
         <div>
            <NavBar/>
            <div style={{marginTop: 50 + "px"}} className="container">
               <Route path="/" exact component={Home}/>
               <Route path="/list" component={List}/>
               <Route path="/resource/:id" component={Resource}/>
            </div>
         </div>
      )
   }
}
