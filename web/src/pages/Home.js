import React, {Component} from "react";
import Grid from "material-ui/es/Grid/Grid";

export default class Home extends Component {
   constructor(props) {
      super(props);
   }

   render() {
      return (
            <Grid container spacing={8}>
               <Grid item xs={12} style={{textAlign: "center"}}>
                  <h1>Welcome!</h1>
                  <h3>Hello. It`s a home page of the semantic task.</h3>
               </Grid>
            </Grid>

      )
   }
}
