import React, {Component} from "react";

import {
   Divider,
   Grid, Paper, Typography
} from "material-ui";

import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import BandList from "./component/BandList";
import ArtistList from "./component/ArtistList";

const styles = {};

class List extends Component {

   constructor(props) {
      super(props);
      this.state = {}
   }

   render() {
      const {classes} = this.props;
      return (
         <div>
            <Grid container spacing={8}>
               <Grid item xs={12}>
                  <h1 style={{textAlign: "center"}}>Content</h1>
               </Grid>
            </Grid>
            <div>
               <Paper className={classes.root} elevation={4}>
                  <div>
                     <div>
                        <h3 style={{textAlign: "center"}}>Artists</h3>
                        <Divider light/>
                        <ArtistList/>
                     </div>
                     <div>
                        <h3 style={{textAlign: "center"}}>Bands</h3>
                        <Divider light/>
                        <BandList/>
                     </div>
                  </div>
               </Paper>
            </div>

         </div>
      );
   }
}

List.propTypes = {
   classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(List);