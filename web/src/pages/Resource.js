import React, {Component} from "react";

import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import {Grid} from "material-ui";
import {getLinkedFrom, getResource} from "../actions/actions";
import {Link} from "react-router-dom";
import {getURIName, isURI, isURL, parsePropLang, parsePropType, parsePropValue} from "../actions/helpers";

const styles = {
   property: {
      name: {
         fontWeight: "bold"
      },
      lang: {
         fontStyle: "italic"
      },
      value: {}
   }
};

class Resource extends Component {

   constructor(props) {
      super(props);
      this.state = {
         resource: null,
         linked: null
      }
   }

   componentWillReceiveProps = (props) => {
      this.setState({
         resource: null,
         linked: null
      });
   };


   updateResource = (id) => {
      getResource(
         id,
         (error, result) => {
            this.setState({
               resource: result
            });
            console.log(result);
            this.updateLinked(id);
         }
      )
   };

   updateLinked = (id) => {
      getLinkedFrom(
         "music:" + id,
         (error, result) => {
            this.setState({
               linked: result
            });
            console.log(result);
         }
      )
   };

   createValueComponent = (id, prop) => {
      let value = parsePropValue(prop);
      if (isURL(value) && !isURI(value)) {
         return <a href={value} style={{pointerId: "finger"}}>
            <span> {value.replace("Has","")}</span>
         </a>
      } else if (isURI(value)) {
         let resourceName = getURIName(value);
         return <Link to={"/resource/" + resourceName} style={{pointerId: "finger"}}>
            <span> {value}</span>
         </Link>
      } else {
         return <span> {value}</span>
      }
   };

   getRenderedProp = (prop) => {
      const {classes} = this.props;
      const {id} = this.props.match.params;
      return (
         <div>
                <span style={{fontWeight: "bold"}}>
                     {parsePropType(prop)} {parsePropLang(prop)}:
                </span>
            {
               this.createValueComponent(id, prop)
            }

         </div>
      );
   };

   render() {
      const {classes} = this.props;
      const {id} = this.props.match.params;

      if (this.state.resource === null || this.state.linked === null) {
         this.updateResource(id);
         return null;
      }
      return (
         <div>
            <div style={{display: "block", margin: "0 auto", width: 800}}>
               <Grid container spacing={8}>
                  <Grid item xs={12} style={{textAlign: "center"}}>
                     <h1>{id}</h1>
                  </Grid>
                  {
                     this.state.resource.results.bindings.map(
                        (prop, i) =>
                           <Grid item xs={12} key={"res_" + i}>
                              {this.getRenderedProp(prop, i)}
                           </Grid>
                     )
                  }

                  <Grid item xs={12}>
                     {
                        this.state.linked.results.bindings.map(
                           (prop, i) =>
                              <Grid item xs={12} key={"res_" + i}>
                                 {this.getRenderedProp(prop, i)}
                              </Grid>
                        )
                     }
                  </Grid>
               </Grid>

            </div>
         </div>

      );
   }
}

Resource.propTypes = {
   classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Resource);