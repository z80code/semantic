import React, {Component} from 'react'
import {Nav, Navbar, NavItem} from "react-bootstrap";

import {Page} from "../../constants/constants";
import {LinkContainer} from "react-router-bootstrap";

export default class NavBar extends Component {

   constructor(props) {
      super(props);
      this.state = {
         selectedPage: Page.HOME,
      }
   }

   onSelect = (eventKey) => {
      this.setState({
         selectedPage: eventKey
      });
   };

   render() {
      return (
         <Navbar inverse collapseOnSelect fixedTop onSelect={this.onSelect}>
            <Navbar.Header>
               <Navbar.Brand>
                  <LinkContainer to="/">
                     <div>Semantic</div>
                  </LinkContainer>
               </Navbar.Brand>
               <Navbar.Toggle/>
            </Navbar.Header>
            <Navbar.Collapse>
               <Nav>
                  <LinkContainer to="/list">
                     <NavItem eventKey={Page.LIST}
                              active={this.state.selectedPage === Page.LIST}
                     >LIST</NavItem>
                  </LinkContainer>
                  <LinkContainer to="/resource">
                     <NavItem eventKey={Page.RESOURCE}
                              active={this.state.selectedPage === Page.RESOURCE}
                     >RESOURCE</NavItem>
                  </LinkContainer>
               </Nav>
            </Navbar.Collapse>
         </Navbar>
      );
   }
}