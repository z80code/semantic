import React, {Component} from "react";

import Pagination from "react-js-pagination";
import imageGif from "../../images/loading.gif";

import {getBands} from "../../actions/actions";
import {Link} from "react-router-dom";
import BandUnit from "./BandUnit";

const defaultProperties = {
   start: 0,
   rows: 3,
};

export default class BandList extends Component {
   constructor(props) {
      super(props);
      this.state = {
         properties: defaultProperties,
         activePage: 1,
         requestResult: null,
         error: false
      }
   }

   componentWillReceiveProps(newProps) {
      this.setState({
         requestResult: null
      });
   };

   handlePageChange = (pageNumber) => {
      let prop = this.state.properties;
      prop.start = (+pageNumber - 1) * this.state.properties.rows;
      this.setState({
         properties: prop,
         activePage: pageNumber
      });
      this.searchRequest();
   };

   getBandsRequest = () => {
      getBands(
         this.state.properties.start,
         (error, result) => {
            this.setState({
               error: error,
               requestResult: result
            });
         });
   };


   onViewClicked = (doc) => {
      this.setState({viewDoc: doc});
   };

   closeViewer = () => {
      this.setState({viewDoc: null});
   };

   render() {

      if (this.state.requestResult === null) {
         this.getBandsRequest();
         return (
            <div style={{margin: "0 auto", width: "150px", textAlign: "center"}}>
               <img src={imageGif} alt="Loading..."/>
            </div>
         );
      }

      if (!this.state.requestResult) return null;

      let requestResult = this.state.requestResult;

      let pagination = "";

      if (requestResult.results.bindings.length !== 0) {
         pagination = <div style={{textAlign: "center", clear: "both"}}>
            <Pagination activePage={this.state.activePage}
                        itemsCountPerPage={this.state.properties.rows}
                        totalItemsCount={requestResult.results.bindings.lenght}
                        pageRangeDisplayed={5}
                        onChange={this.handlePageChange}/>
         </div>;
      }
      return (
         <div style={{clear: "both"}}>
             <div>
               {pagination}
               {
                  requestResult.results.bindings.map((unit, i) =>
                     <BandUnit document={unit} key={"unit_result_item_" + i} onView={this.onViewClicked}/>
                  )
               }
               {pagination}
            </div>
         </div>
      );
   }
}