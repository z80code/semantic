import React, {Component} from 'react'

import {Button, Card, CardContent, Typography} from "material-ui";


import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';

import {defImage} from "../../constants/constants"
import {getImage} from "../../actions/actions";
import imageGif from "../../images/loading.gif";
import {LinkContainer} from "react-router-bootstrap";
import {Link} from "react-router-dom";

const styles = {
   bookCard: {
      width: "300px",
      float: "left",
      padding: "0 10px",
      margin: "10px 0"
   },
   header: {
      title: {
         fontSize: "1.2em"
      },
      ranking: {
         fontSize: "1em"
      },
      height: "80px",
      overflow: "hidden",
      backgroundColor: "#eee",
      marginBottom: 5
   },
   image: {
      textAlign: "center",
      height: "196px",
      justifyContent: "middle"
   },
   description: {
      height: 110,
      background: "#eee",
      padding: "5px",
      overflow: "hidden"
   },
   openButton: {
      width: "100%",
      fontSize: "1em"
   }
};

class BandUnit extends Component {

   constructor(props) {
      super(props);
      this.state = {
         image: null
      }
   }

   componentWillReceiveProps(nextProps) {
      if (this.props.document.image !== nextProps.document.image) {
         this.setState({image: null});
      }
   }

   updateImage = () => {
      getImage(this.props.document.image,
         (image) => {
            // if (image === null) {
            //    this.setState({image: defImage});
            // } else {
            //    this.setState({
            //       image: (image.indexOf("data") === 0 ? "" : 'data:image/jpeg;base64, ') + image
            //    });
            // }
         }
      );
   };

   arrayPreProcessing = (arr) => {
      let result = arr = arr.filter(function (value, index, array) {
         return array.indexOf(value) == index;
      });
      console.log(result);
      return result;
   };


   render() {
      const {classes} = this.props;

      let doc = this.props.document;
      if (this.state.image === null) {
         // this.updateImage();
      }
      console.log(doc);
      let styleNames = JSON.parse(doc.styleNames.value);
      let members = JSON.parse(doc.members.value);
      return (
         <div className={classes.bookCard}>
            <Card>
               <Link to={"/resource/" + doc.bandId.value}>
                  <Button color="default" className={classes.openButton}>
                     {doc.bandTitle.value}
                  </Button>
               </Link>
               <div className={classes.image}>
                  <img alt={doc.title} title={doc.title} src={doc.image.value || imageGif}
                       style={{width: "90%", margin: "5px auto"}}/>
               </div>
               <CardContent className={classes.header}>
                  <div>
                     <span>Title: </span>
                     <Link to={"/resource/" + doc.bandId.value}
                           className={classes.header.title}>
                        {/*<Typography type="headline">*/}
                        {doc.bandTitle.value}
                        {/*</Typography>*/}
                     </Link>
                  </div>
                  <div>
                     <span> Styles: </span>
                     {
                        this.arrayPreProcessing(styleNames).map((style, i) =>
                              <span key={"k_" + i}>
                           <Link to={"/resource/" + style.replace(" ", "_")}>{style}</Link>
                                 {styleNames.length > i ? ", " : ""}
                        </span>
                        )
                     }
                  </div>
                  <div>
                     <span>Members: </span>
                     {
                        this.arrayPreProcessing(members).map((style, i) =>
                              <span key={"k_" + i}>
                           <Link to={"/resource/" + style.replace(" ", "_")}>{style}</Link>
                                 {styleNames.length > i ? ", " : ""}
                        </span>
                        )
                     }
                  </div>
               </CardContent>
               {
                  this.props.content || null
               }
            </Card>
         </div>
      );
   }
}

BandUnit.propTypes = {
   classes: PropTypes.object.isRequired,
   document: PropTypes.object.isRequired
};

export default withStyles(styles)(BandUnit);