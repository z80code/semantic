import React, {Component} from 'react'

import {Button, Card, CardContent, Typography} from "material-ui";

import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';

import {defImage} from "../../constants/constants"
import {getImage} from "../../actions/actions";
import imageGif from "../../images/loading.gif";
import BandUnit from "./BandUnit";

const styles = {
   bookCard: {
      width: "300px",
      float: "left",
      padding: "0 10px",
      margin: "10px 0"
   },
   header: {
      title: {
         fontSize: "0.8em"
      },
      author: {
         fontSize: "0.8em"
      },
      overflow: "hidden",
      height: "72px",
      backgroundColor: "#eee",
      marginBottom: 5
   },
   image: {
      textAlign: "center",
      height: "140px",
      justifyContent: "middle"
   },
   description: {
      height: "110px",
      background: "#eee",
      padding: "5px",
      overflow: "hidden"
   },
   openButton: {
      width: "100%"
   }
};

class ViewBandUnit extends Component {

   constructor(props) {
      super(props);
      // this.state = {
      //    image: null
      // }
   }

   createDescription = (doc) => {
      const {classes} = this.props;
      return (
         <div className={classes.description}>
            {/*<div><i>Publisher:</i> {doc.publisher}</div>*/}
            {/*<div><i>Created:</i> {doc.creationDate.slice(0, 4) === "1970" ? "" : doc.creationDate.slice(0, 4)}*/}
            {/*</div>*/}
            {/*<div><i>Genre:</i> {doc.genres.join(", ")}</div>*/}
            {/*<div><i>Annotation:</i> {doc.anotation}</div>*/}
         </div>   );
   };

   //
   // componentWillReceiveProps(nextProps) {
   //    if (this.props.document.image !== nextProps.document.image) {
   //       this.setState({image: null});
   //    }
   // }

   // updateImage = () => {
   //    getImage(this.props.document.image,
   //       (image) => {
   //          if (image === null) {
   //             this.setState({image: defImage});
   //          } else {
   //             this.setState({
   //                image: (image.indexOf("data") === 0 ? "" : 'data:image/jpeg;base64, ') + image
   //             });
   //          }
   //       }
   //    );
   // };

   openViewBook = (doc) => {
      this.props.onView(doc);
   };

   render() {
      const {classes} = this.props;

      let doc = this.props.document;
      // console.log(doc);
      // if (this.state.image === null) {
      //    this.updateImage();
      // }

      return (
         <BandUnit document={doc} onView={this.openViewBook} content={
            this.createDescription(doc)
         }/>
         //
         // <div className={classes.bookCard}>
         //    <Card>
         //       <div>
         //          <CardContent className={classes.header}>
         //             <Typography type="headline" className={classes.header.title}> {doc.title}</Typography>
         //             <Typography type="subheading" color="secondary" className={classes.header.author}>
         //                {doc.authors.join(", ")}
         //             </Typography>
         //          </CardContent>
         //          <div className={classes.image}>
         //             <img alt={doc.title} title={doc.title} src={this.state.image || imageGif}
         //                  style={{width: "90%", margin: "0 auto"}}/>
         //          </div>
         //          {
         //             this.createDescription(doc)
         //          }
         //          <Button color="default" onClick={this.openViewBook} className={classes.openButton}>
         //             View
         //          </Button>
         //       </div>
         //    </Card>
         // </div>
      );
   }
}


ViewBandUnit.propTypes = {
   classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ViewBandUnit);