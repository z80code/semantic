import React, {Component} from "react";

import Pagination from "react-js-pagination";
import imageGif from "../../images/loading.gif";

import {getArtists, getArtistsCount} from "../../actions/actions";
import {Link} from "react-router-dom";
import ArtistUnit from "./ArtistUnit";

const defaultProperties = {
   start: 0,
   rows: 3,
};

export default class ArtistList extends Component {
   constructor(props) {
      super(props);
      this.state = {
         properties: defaultProperties,
         activePage: 1,
         count: 1,
         requestResult: null,
         error: false
      }
   }

   componentWillReceiveProps(newProps) {
      this.setState({
         requestResult: null,
         properties: defaultProperties,
         count: 0
      });
   };

   handlePageChange = (pageNumber) => {
      let prop = this.state.properties;
      prop.start = (+pageNumber - 1) * this.state.properties.rows;
      console.log( prop.start);
      this.setState({
         properties: prop,
         requestResult: null,
         activePage: pageNumber
      });
   };

   getArtistsRequest = () => {
      getArtistsCount((error, result) => {
         let count = +result.results.bindings[0].count.value;
         console.log(count);
         getArtists(
            this.state.properties.start,
            (error, result) => {
               this.setState({
                  count: count,
                  error: error,
                  requestResult: result
               });
               console.log(result);
            });
      })
   };

   render() {
      if (this.state.requestResult === null) {
         this.getArtistsRequest();
         return (
            <div style={{margin: "0 auto", width: "150px", textAlign: "center"}}>
               <img src={imageGif} alt="Loading..."/>
            </div>
         );
      }

      if (!this.state.requestResult) return null;

      let requestResult = this.state.requestResult;

      let pagination = "";

      // let itemsCount = (this.state.count / this.state.properties.rows +
      // (this.state.count / this.state.properties.rows) % this.state.properties.rows ? 0 : 1);

      if (requestResult.results.bindings.length !== 0) {
         pagination = <div style={{textAlign: "center", clear: "both"}}>
            <Pagination activePage={this.state.activePage}
                        itemsCountPerPage={this.state.properties.rows}
                        totalItemsCount={this.state.count}
                        pageRangeDisplayed={5}
                        onChange={this.handlePageChange}/>
         </div>;
      }
      return (
         <div style={{clear: "both"}}>
            <div>
               {pagination}
               {
                  requestResult.results.bindings.map((unit, i) =>
                     <ArtistUnit document={unit} key={"unit_result_item_" + i} onView={this.onViewClicked}/>
                  )
               }
               {pagination}
            </div>
         </div>
      );
   }
}