import React, {Component} from 'react'

import {Button, Card, CardContent, Typography} from "material-ui";

import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';

import {defImage} from "../../constants/constants";
import {getImage} from "../../actions/actions";
import imageGif from "../../images/loading.gif";
import ArtistUnit from "./ArtistUnit";

const styles = {
   bookCard: {
      width: "300px",
      float: "left",
      padding: "0 10px",
      margin: "10px 0"
   },
   header: {
      title: {
         fontSize: "0.8em"
      },
      author: {
         fontSize: "0.8em"
      },
      overflow: "hidden",
      height: "72px",
      backgroundColor: "#eee",
      marginBottom: 5
   },
   image: {
      textAlign: "center",
      height: "140px",
      justifyContent: "middle"
   },
   description: {
      height: "110px",
      background: "#eee",
      padding: "5px",
      overflow: "hidden"
   },
   openButton: {
      width: "100%"
   }
};

class ViewArtistList extends Component {

   constructor(props) {
      super(props);
      // this.state = {
      //    image: null
      // }
   }

   createDescription = (doc) => {
      const {classes} = this.props;
      return (
         <div className={classes.description}>
            {/*<div><i>Publisher:</i> {doc.publisher}</div>*/}
            {/*<div><i>Created:</i> {doc.creationDate.slice(0, 4) === "1970" ? "" : doc.creationDate.slice(0, 4)}*/}
            {/*</div>*/}
            {/*<div><i>Genre:</i> {doc.genres.join(", ")}</div>*/}
            {/*<div><i>Annotation:</i> {doc.anotation}</div>*/}
         </div>   );
   };

   openViewBook = (doc) => {
      this.props.onView(doc);
   };

   render() {
      const {classes} = this.props;

      let doc = this.props.document;
      // console.log(doc);
      // if (this.state.image === null) {
      //    this.updateImage();
      // }

      return (
         <ArtistUnit document={doc} onView={this.openViewBook} content={
            this.createDescription(doc)
         }/>
      );
   }
}


ViewArtistList.propTypes = {
   classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ViewArtistList);