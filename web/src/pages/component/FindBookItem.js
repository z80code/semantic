import React, {Component} from 'react'

import BookItem from "./BandUnit";

import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';

const styles = {};

class FindBookItem extends Component {

   constructor(props) {
      super(props);
   }

   prepareHighlighting = (hgObject) => {
      let counter = 0;
      let result = "";
      for (let key in hgObject) {
         counter++;
         result += "<div>" + counter + ") <span><b>" + key + ": </b>" + hgObject[key] + "</span></div>";
      }
      return result;
   };

   openViewBook = (doc) => {
      this.props.onView(doc);
   };

   render() {
      let doc = this.props.document;

      if (doc === null) {
        return null;
      }
      return (
         <BookItem document={doc} onView={this.openViewBook} content={
            <div style={{height: "200px", margin: "5px", overflowY: "scroll"}}>
               <div style={{background: "#eee", margin: "0 5px"}}>Hits:</div>
               <div dangerouslySetInnerHTML={{__html: this.prepareHighlighting(doc.highlighting)}}/>
            </div>
         }/>
      );
   }
}

FindBookItem.propTypes = {
   classes: PropTypes.object.isRequired,
   onView: PropTypes.func.isRequired,
   document: PropTypes.object.isRequired
};

export default withStyles(styles)(FindBookItem);