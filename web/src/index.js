import React from 'react'
import ReactDom from 'react-dom'
import Root from "./containers/Root";

import 'bootstrap/dist/css/bootstrap.min.css'

ReactDom.render(
    <Root/>,
    document.getElementById('root')
);