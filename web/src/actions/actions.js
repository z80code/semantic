import $ from "jquery";

function getBandsCount(callback) {

   let SparqlClient = require('sparql-client');
   let util = require('util');
   let endpoint = 'http://localhost:3030/music/query';

   let query = `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                PREFIX music: <http://www.semanticweb.org/scorpion/ontologies/2018/0/music#>
                PREFIX xml: <http://www.w3.org/XML/1998/namespace/>
      
      SELECT (COUNT(?bandId) AS ?count)
	
      WHERE {
          ?band a music:Band . 
          ?band music:id ?bandId . 
          ?band music:bandName ?bandTitle .
          ?band music:image ?image . 
          ?band music:hasStyle ?style .
          ?style music:styleName ?styleName .
          ?band music:hasMember ?member .
          ?member music:personName ?memberName .
          FILTER (lang(?memberName) = 'en') .
          FILTER (lang(?bandTitle) = 'en') .
      }`;

   let client = new SparqlClient(endpoint);

   console.log("Query to " + endpoint);
   console.log("Query: " + query);
   client.query(query)
      .execute(function (error, results) {
         console.log("Results: " + results);
         callback(error, results);
      });

}

function getBands(start, callback) {

   let SparqlClient = require('sparql-client');
   let util = require('util');
   let endpoint = 'http://localhost:3030/music/query';

   let query = `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                PREFIX music: <http://www.semanticweb.org/scorpion/ontologies/2018/0/music#>
                PREFIX xml: <http://www.w3.org/XML/1998/namespace/>
             
                SELECT ?bandId 
	   ?bandTitle 
	   ?image
	   (concat('["', group_concat(?styleName;separator='", "'), '"]') as ?styleNames)
	   (concat('["', group_concat(?memberName;separator='", "'), '"]') as ?members)

      WHERE {
          ?band a music:Band . 
          ?band music:id ?bandId . 
          ?band music:bandName ?bandTitle .
          ?band music:image ?image . 
          ?band music:hasStyle ?style .
          ?style music:styleName ?styleName .
          ?band music:hasMember ?member .
          ?member music:personName ?memberName .
          FILTER (lang(?memberName) = 'en') .
          FILTER (lang(?bandTitle) = 'en') .
      }
      GROUP BY ?bandId ?bandTitle ?image
      ORDER BY ?bandId
      OFFSET  ${start}
      LIMIT 3`;

   let client = new SparqlClient(endpoint);

   console.log("Query to " + endpoint);
   console.log("Query: " + query);
   client.query(query)
      .execute(function (error, results) {
         console.log("Results: " + results);
         callback(error, results);
      });

}

function getArtistsCount(callback) {

   let SparqlClient = require('sparql-client');
   let util = require('util');
   let endpoint = 'http://localhost:3030/music/query';

   let query = `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                PREFIX music: <http://www.semanticweb.org/scorpion/ontologies/2018/0/music#>
                PREFIX xml: <http://www.w3.org/XML/1998/namespace/>
             
               SELECT (COUNT(?artistId) AS ?count)
                      WHERE {
                      ?artist a music:Artist . 
                      ?artist music:id ?artistId . 
                      ?artist music:personName ?artistTitle .
                      ?artist music:image ?image . 
                      ?artist music:hasLocation ?location . 
                      ?location music:locationName ?locationName . 
                      ?artist music:hasStyle ?style .
                      ?style music:styleName ?styleName .
                      ?artist music:hasRole ?role .
                      ?role music:roleName ?roleName .
                      ?artist music:playsOn ?instrument .
                      ?instrument music:instrumentName ?instrumentName .
                      FILTER (lang(?locationName) = 'en') .
                      FILTER (lang(?instrumentName) = 'en') .
                      FILTER (lang(?styleName) = 'en') .
                      FILTER (lang(?roleName) = 'en') .
                      FILTER (lang(?artistTitle) = 'en') .
                     }`;

   let client = new SparqlClient(endpoint);

   console.log("Query to " + endpoint);
   console.log("Query: " + query);
   client.query(query)
      .execute(function (error, results) {
         console.log("Results: " + results);
         callback(error, results);
      });

}

function getArtists(start, callback) {

   let SparqlClient = require('sparql-client');
   let util = require('util');
   let endpoint = 'http://localhost:3030/music/query';

   let query = `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                PREFIX music: <http://www.semanticweb.org/scorpion/ontologies/2018/0/music#>
                PREFIX xml: <http://www.w3.org/XML/1998/namespace/>
             
               SELECT ?artistId 
                     ?artistTitle 
                     ?image
                     ?locationName
                     (concat('["', group_concat(?styleName;separator='", "'), '"]') as ?styleNames)
                     (concat('["', group_concat(?roleName;separator='", "'), '"]') as ?roleNames)
                     (concat('["', group_concat(?instrumentName;separator='", "'), '"]') as ?instrumentNames)
                     WHERE {
                         ?artist a music:Artist . 
                         ?artist music:id ?artistId . 
                         ?artist music:personName ?artistTitle .
                         ?artist music:image ?image . 
                       ?artist music:hasLocation ?location . 
                       ?location music:locationName ?locationName . 
                         ?artist music:hasStyle ?style .
                         ?style music:styleName ?styleName .
                         ?artist music:hasRole ?role .
                         ?role music:roleName ?roleName .
                         ?artist music:playsOn ?instrument .
                         ?instrument music:instrumentName ?instrumentName .
                         FILTER (lang(?locationName) = 'en') .
                         FILTER (lang(?instrumentName) = 'en') .
                         FILTER (lang(?styleName) = 'en') .
                         FILTER (lang(?roleName) = 'en') .
                         FILTER (lang(?artistTitle) = 'en') .
                     }
                     GROUP BY ?artistId ?artistTitle ?image ?locationName
                     ORDER BY ?artistId
                     OFFSET  ${start}
                     LIMIT 3`;

   let client = new SparqlClient(endpoint);

   console.log("Query to " + endpoint);
   console.log("Query: " + query);
   client.query(query)
      .execute(function (error, results) {
         console.log("Results: " + results);
         callback(error, results);
      });

}


function getResource(id, callback) {

   let SparqlClient = require('sparql-client');
   let util = require('util');
   let endpoint = 'http://localhost:3030/music/query';

   let query = `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
               PREFIX music: <http://www.semanticweb.org/scorpion/ontologies/2018/0/music#>
               PREFIX xml: <http://www.w3.org/XML/1998/namespace/>
               PREFIX owl: <http://www.w3.org/2002/07/owl#>
               
               SELECT ?prop ?o 
               WHERE { 
               ?unit music:id "${id}" . 
                ?unit ?prop ?o . 
               }
               ORDER BY ?prop `;

   let client = new SparqlClient(endpoint);

   console.log("Query to " + endpoint);
   console.log("Query: " + query);
   client.query(query)
      .execute(function (error, results) {
         console.log("Results: " + results);
         callback(error, results);
      });

}

function getLinkedFrom(name, callback) {

   let SparqlClient = require('sparql-client');
   let util = require('util');
   let endpoint = 'http://localhost:3030/music/query';

   let query = `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                PREFIX music: <http://www.semanticweb.org/scorpion/ontologies/2018/0/music#>
                PREFIX xml: <http://www.w3.org/XML/1998/namespace/>
             
                SELECT ?prop ?o 
                      WHERE {
                         ?o ?p ${name}
                      }
                      ORDER BY ?p
                      `;

   let client = new SparqlClient(endpoint);

   console.log("Query to " + endpoint);
   console.log("Query: " + query);
   client.query(query)
      .execute(function (error, results) {
         console.log("Results: " + results);
         callback(error, results);
      });

}

export {getBands, getArtists, getResource, getLinkedFrom, getArtistsCount, getBandsCount};