const ANCHOR_URI = "(#){1}(\S)*";
const ANCHOR_URL = "(http|ftp|https):\\/\\/";

function isURI(uri) {

   return !!~uri.search(ANCHOR_URI);
}

function isURL(uri) {

   return !!~uri.search(ANCHOR_URL);
}

const SHORT_NAME_PROP = "(?<=#)(.)*";

function getURIName(uri) {
   let match = uri.match(SHORT_NAME_PROP);
   return match["0"] || "";
}
function getURILongName(uri) {
   let arr = uri.split(/\//);
   return  arr[arr.length-1] || "";
}

function parsePropType(prop) {
   let uri = prop.prop?prop.prop.value:null;
   if(!uri) return "Linked to";
   let val = getURIName(uri);
   let val1 = val
      .replace(/^([a-z]|[a-zA-Z])([A-Za-z]$)/, '$1 $2');

   return (val1.charAt(0).toUpperCase() + val.slice(1)).replace("Has", "");
}


function parsePropLang(prop) {
   return prop.o["xml:lang"] ? "(" + prop.o["xml:lang"] + ")" : "";
}

function parsePropValue(prop) {
   return prop.o.value;
}


export {isURI, isURL, parsePropType, parsePropLang, parsePropValue, getURIName, getURILongName}