let path = require('path');
let webpack = require('webpack');

const PATHS = {
   source: path.join(__dirname, 'src/index.js'),
   output: path.join(__dirname, '../web-app/src/main/webapp/js')
};

module.exports = {
   entry: PATHS.source,
   output: {
      path: PATHS.output,
      filename: "bundle.js"
   },
   module: {
      loaders: [
         {
            test: /\.js?$|\.jsx?$/,
            loader: 'babel-loader',
            exclude: /node_modules/
         },
         {
            test: /\.css?$/,
            loader: "style-loader!css-loader",
         },
         {
            test: /\.scss$/,
            loaders: ["style-loader", "css-loader", "sass-loader"]
         },
         {
            test: /\.less$/,
            loader: "style-loader!css-loader!less-loader!autoprefixer-loader!less",
            exclude: [/node_modules/, /public/]
         },
         {
            test: /\.svg/,
            loader: "url-loader?limit=2600000&mimetype=image/svg+xml"
         },
         {
            test: /\.jsx$/,
            loader: "react-hot!babel",
            exclude: [/node_modules/]
         },
         {
            test: /\.json$/,
            loader: "json-loader"
         },
         {
            test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            loader: 'url-loader'
         },
         {
            test: /\.(ttf|otf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?|(jpg|gif|png)$/,
            loader: 'url-loader'
         }
      ],
   },
    node: {
        console: true,
        fs: 'empty',
        net: 'empty',
        tls: 'empty'
    },
   // plugins: [
   //     new webpack.optimize.UglifyJsPlugin({
   //         beautify: false,
   //         comments: false,
   //         compress: {
   //             sequences   : true,
   //             booleans    : true,
   //             loops       : true,
   //             unused      : true,
   //             warnings    : false,
   //             drop_console: true,
   //             unsafe      : true
   //         },
   //         output: {
   //             ascii_only: true
   //         }
   //     }),
   //     new webpack.DefinePlugin({
   //         'process.env': {
   //             NODE_ENV: JSON.stringify('production')
   //         }
   //     }),
   //     // new webpack.NoEmitOnErrorsPlugin(),
   //     new webpack.optimize.OccurrenceOrderPlugin(),
   //     new webpack.optimize.CommonsChunkPlugin({
   //         children: true,
   //         async: true,
   //     })
   // ],

   // devServer: {
   //     noInfo: false,
   //     quiet: false,
   //     lazy: false,
   //     watchOptions: {
   //         poll: true
   //     }
   // }
};

